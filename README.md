# RepertoireExperiment
The `RepertoireExperiment` class extends the `MultiAssayExperiment` class to
better capture data from T and B cell repertoire sequencing experiments.

## Philosophy
The philosophy of `RepertoireExperiment` is that joining all the data from the
experiment at the beginning is better than doing it on the fly. This allows
comparative metrics, public clones, and a variety of other analyses to be done
more quickly. The data is stored as a sparse `Matrix`, so there is very little
overhead. The object also keeps the metadata and data linked together, and
allows for nucleotide and amino acid level representations of the data to be
stored simultaneously.   

## Importing Data 
 If you are starting with ImmunoSeq data from Adaptive, then you should import
the data using `parseAdaptive`.
```R
#make a list of tsv files
# path <- /path/to/tsvs
fl <- list.files(path, pattern='*.tsv', full.name=T)

#Parse the data into a data.frame
df <- parseAdaptive(fl)
``` 
Then, the load a metadata dictionary with the following basic format. 

fn | patient | type | response
---|---------|------|---------- 
  samplept1post|       1|  Post|        R
   samplept1pre|       1|   Pre|        R
 samplept1tumor|       1| Tumor|        R
  samplept2post|       2|  Post|       NR
   samplept2pre|       2|   Pre|       NR
 samplept2tumor|       2| Tumor|       NR

The only necessary feature is the `fn` column which should be the filenames of
the original tsv files. 

The `data.frame` from `parseAdaptive` and the dictionary are all that you need
to make a `RepertoireExperiment` object.

```R
re <- MakeRepertoireExperiment(df,dict) 

```
An amino acid level representation can be created by aggregating receptors with
synonymous nucleotide sequences. 

```R
#generate a Amino Acid level representation (aggregate synonymous TCRs)
re <- aggregateRepertoire(re)
```

And various metrics can be calculated and stored back inside the object's
metadata

```R
#calculate some metrics and save into the object
re <- clonality(re)
re <- richness(re)
re <- total(re)
```
