% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/data.R
\docType{data}
\name{repertoire}
\alias{repertoire}
\title{A TCR Repertoire experiment}
\format{A RepertoireExperiment object}
\usage{
repertoire
}
\description{
This is a heavily down-sampled and randomized data set from a real cohort of
pancreatic ductal adenocarcinoma patients. The receptors and their counts are
real, but they have been mixed and re-sampled to assure that no real patient
data is presented. The metadata is entirely made up (sex, response, etc.)
}
\keyword{datasets}
